#!/bin/bash

cp -r HistFitter_modifications/scripts/* HistFitter/scripts
cp -r HistFitter_modifications/src/* HistFitter/src
cp -r HistFitter_modifications/python/* HistFitter/python
cp -r HistFitter_modifications/analysis/* HistFitter/analysis

cp HistFitter_config/dump.py HistFitter/
cp HistFitter_config/runfits.sh HistFitter/
cp HistFitter_config/Wt_MET_1L.py HistFitter/analysis/
cp HistFitter_config/contourPlotterExample.py

cd HistFitter/src
make
cd ../
source setup.sh
