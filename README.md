### Overview

Updated: 18/10/19

I've tried to automate as much of this Gitlab as possible. There are three scripts:

clean_histfitter.sh: This script will remove HistFitter from this Gitlab, in case you want to do a fresh install.
get_histfitter.sh: This script will add HistFitter as a submodule in this Git repo, checkout the latest version (v63) and build everything.
setup_histfitter.sh: This script will copy the custom tW+MET analyses to the analysis folder.

This has been tested as working on both Liverpool systems and lxplus. 

### Setup at Liverpool

After cloning the respository, do the following:

```
./clean_histfitter.sh
. ./get_histfitter.sh
. ./setup_histfitter.sh
```

### Setup on Lxplus

After cloning the respository, do the following:

```
setupATLAS
lsetup 'root 6.06.06-x86_64-slc6-gcc49-opt'
./clean_histfitter.sh
. ./get_histfitter.sh
. ./setup_histfitter.sh
```

Make sure to use these EXACT commands (. ./ is correct!). 

This will clone HistFitter, checkout v63 and build, and then finally patch my modifications and rebuild. 

### Running fits

Inside the HistFitter folder there will be a script named 'runfits.sh'. To run background-only and exclusion fits, both asimov and with real data, do:

```
./runfits.sh
```

The general command to run a background-only fit is:

```
HistFitter.py -w -f -F bkg -D "before,after" -u "asimov validation" analysis/Wt_MET_1L.py
```

"-u asimov validation" will run as an asimov fit and plot the MET and lep1_charge distributions in the CRs and VRs. To run on real data change "-u asimov validation" to "-u validation".

The general command to run an exclusion fit is:

```
HistFitter.py -w -f -F excl -p -D "before,after" -u "asimov" analysis/Wt_MET_1L.py
```

"-u asimov" will run as an asimov fit and plot the MET and lep1_charge distributions in the CRs. To run on real data remove "-u asimov".

Running on lxplus, you should change the file paths to: "/eos/user/m/msulliva/tW+MET/tuples_summer/MChadd".

### Dumping yields tables, contours, fit parameter pulls

There is a script in HistFitter/ named 'dump.py'. Simple comment in/out the fits you want to dump things for and then run:

```
python dump.py
```