#!/bin/bash

git submodule add https://gitlab.cern.ch/HistFitter/HistFitter.git
cd HistFitter/
git checkout -b v0.63.0 v0.63.0
source setup.sh
cd src/
make
cd ../../
