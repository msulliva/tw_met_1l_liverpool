"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Example pull plot based on the pullPlotUtils module. Adapt to create      *
 *      your own style of pull plot. Illustrates all functions to redefine to     *
 *      change labels, colours, etc.                                              * 
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

#!/usr/bin/env python

import ROOT
from ROOT import *
ROOT.PyConfig.IgnoreCommandLineOptions = True
gSystem.Load("libSusyFitter.so")
#gROOT.Reset()
ROOT.gROOT.SetBatch(True)

import os, pickle, subprocess

import pullPlotUtils
from pullPlotUtils import makePullPlot 

# Build a dictionary that remaps region names
def renameRegions():
    myRegionDict = {}

    # Remap region names using the old name as index, e.g.:
    myRegionDict["tW1L_CRtt"] = "tW1L_CRtt"
    myRegionDict["tW1L_CRWm"] = "tW1L_CRWm"
    myRegionDict["tW1L_CRWp"] = "tW1L_CRWp"
    
    return myRegionDict

# Build a list with all the regions you want to use
def makeRegionList():
    regionList=[]

    regionList += ["tW1L_CRtt","tW1L_CRWm","tW1L_CRWp","tW1L_VRtt_met","tW1L_VRWm_met","tW1L_VRWp_met"]

    return regionList

# Define the colors for the pull bars
def getRegionColor(name):
    if name.find("tW1L_VRtt_met") != -1: return kOrange - 2
    if name.find("tW1L_VRWm_met") != -1: return kCyan + 2
    if name.find("tW1L_VRWp_met") != -1: return kCyan + 2
    return 1

# Define the colors for the stacked samples
def getSampleColor(sample):
    if sample == "Top":         return kOrange - 2
    if sample == "SingleTop":   return kOrange + 2
    if sample == "Wjets":       return kCyan + 2
    if sample == "Zjets":       return kRed - 4
    if sample == "ttV":         return kAzure - 3
    if sample == "ttH":         return kGray
    if sample == "tWZ":         return kGray
    if sample == "Diboson":     return kMagenta + 4

    else:
        print "cannot find color for sample (",sample,")"

    return 1

def main():
    # Override pullPlotUtils' default colours (which are all black)
    pullPlotUtils.getRegionColor = getRegionColor
    pullPlotUtils.getSampleColor = getSampleColor

    # Where's the workspace file? 
    wsfilename = os.getenv("HISTFITTER")+"/results/monotop_asimov/BkgOnly_combined_NormalMeasurement_model_afterFit.root" # 

    # Where's the pickle file?
    pickleFilename = os.getenv("HISTFITTER")+"/bkgonly.pickle"
    
    # Run blinded?
    doBlind = False

    # Used as plot title
    region = "Pulls"

    # Samples to stack on top of eachother in each region
    samples = "Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ"
    
    # Which regions do we use? 
    regionList = makeRegionList()

    # Regions for which the label gets changed
    renamedRegions = renameRegions()

    if not os.path.exists(pickleFilename):
        print "pickle filename %s does not exist" % pickleFilename
        print "will proceed to run yieldstable again"
        
        # Run YieldsTable.py with all regions and samples requested
        cmd = "YieldsTable.py -c %s -s %s -w %s -o MyYieldsTable.tex" % (",".join(regionList), samples, wsfilename)
        print cmd
        subprocess.call(cmd, shell=True)

    if not os.path.exists(pickleFilename):
        print "pickle filename %s still does not exist" % pickleFilename
        return
    
    # Open the pickle and make the pull plot
    makePullPlot(pickleFilename, regionList, samples, renamedRegions, region, doBlind, plotSignificance=False)

if __name__ == "__main__":
    main()
