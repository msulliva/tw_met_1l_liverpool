import os
from ROOT import TFile

def main():

    dump_bkgonly()
    dump_bkgonly_asimov()

    dump_excl()
    dump_excl_asimov()

def dump_excl():

    # Make directory
    if 'excl' not in os.listdir('.'):
        print('No excl file found')
        os.system('mkdir excl')
    # Dump bkgonly tables - CRs
    #os.system('YieldsTable.py -w results/monotop/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_CRtt,tW1L_CRWm,tW1L_CRWp -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_CRs.tex -C CRs')
    #os.system('pdflatex bkgonly_CRs.tex')
    # Dump bkgonly tables - VRs
    #os.system('YieldsTable.py -w results/monotop/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_VRtt_VR1,tW1L_VRtt_VR2,tW1L_VRW_VR1,tW1L_VRW_VR2 -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_VRs.tex -C VRs')
    #os.system('pdflatex bkgonly_VRs.tex')
    # Dump fit parameters plot
    tfile = TFile('results/monotop_excl/fit_parameters.root')
    fit_parameters = tfile.Get('fit_parameters')
    fit_parameters.SaveAs('fit_parameters_excl.pdf')
    # Dump contour plot
    os.system('GenerateJSONOutput.py -i results/monotop_Output_hypotest.root -f "hypo_ma_%f_mH_%f_tb1_st_0.7" -p "m0:m12"')
    os.system('harvestToContours.py -i monotop_Output_hypotest__1_harvest_list.json -x m0 -y m12')
    os.system('python contourPlotterExample.py')
    os.system('mv monotop_Output_hypotest__1_harvest_list.json excl/')
    os.system('mv outputGraphs.root excl/')
    os.system('mv contourPlotterExample.pdf excl/')
    # Move everything to bkgonly folder
    os.system('mv fit_parameters_excl.pdf excl/')
    #os.system('mv bkgonly_* bkgonly/')

def dump_excl_asimov():

    # Make directory
    if 'excl_asimov' not in os.listdir('.'):
        print('No excl_asimov file found')
        os.system('mkdir excl_asimov')
    # Dump bkgonly tables - CRs
    #os.system('YieldsTable.py -w results/monotop/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_CRtt,tW1L_CRWm,tW1L_CRWp -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_CRs.tex -C CRs')
    #os.system('pdflatex bkgonly_CRs.tex')
    # Dump bkgonly tables - VRs
    #os.system('YieldsTable.py -w results/monotop/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_VRtt_VR1,tW1L_VRtt_VR2,tW1L_VRW_VR1,tW1L_VRW_VR2 -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_VRs.tex -C VRs')
    #os.system('pdflatex bkgonly_VRs.tex')
    # Dump fit parameters plot
    tfile = TFile('results/monotop_asimov_excl/fit_parameters.root')
    fit_parameters = tfile.Get('fit_parameters')
    fit_parameters.SaveAs('fit_parameters_excl_asimov.pdf')
    # Dump contour plot
    os.system('GenerateJSONOutput.py -i results/monotop_asimov_Output_hypotest.root -f "hypo_ma_%f_mH_%f_tb1_st_0.7" -p "m0:m12"')
    os.system('harvestToContours.py -i monotop_asimov_Output_hypotest__1_harvest_list.json -x m0 -y m12')
    os.system('python contourPlotterExample.py')
    os.system('mv monotop_asimov_Output_hypotest__1_harvest_list.json excl_asimov/')
    os.system('mv outputGraphs.root excl_asimov/')
    os.system('mv contourPlotterExample.pdf excl_asimov/')
    # Move everything to bkgonly folder
    os.system('mv fit_parameters_excl_asimov.pdf excl_asimov/')
    #os.system('mv bkgonly_* bkgonly/')

def dump_bkgonly():

    # Make directory
    if 'bkgonly' not in os.listdir('.'):
        print('No bkgonly file found')
        os.system('mkdir bkgonly')
    # Dump bkgonly tables - CRs
    os.system('YieldsTable.py -w results/monotop_bkgonly/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_CRtt,tW1L_CRWm,tW1L_CRWp -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_CRs.tex -C CRs')
    os.system('pdflatex bkgonly_CRs.tex')
    # Dump bkgonly tables - VRs
    os.system('YieldsTable.py -w results/monotop_bkgonly/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_VRtt_VR1,tW1L_VRtt_VR2,tW1L_VRW_VR1,tW1L_VRW_VR2 -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_VRs.tex -C VRs')
    os.system('pdflatex bkgonly_VRs.tex')
    # Dump fit parameters plot
    tfile = TFile('results/monotop_bkgonly/fit_parameters.root')
    fit_parameters = tfile.Get('fit_parameters')
    fit_parameters.SaveAs('fit_parameters_bkgonly.pdf')
    # Move everything to bkgonly folder
    os.system('mv fit_parameters_bkgonly.pdf bkgonly/')
    os.system('mv bkgonly_* bkgonly/')

def dump_bkgonly_asimov():

    # Make directory
    if 'bkgonly_asimov' not in os.listdir('.'):
        print('No bkgonly_asimov file found')
        os.system('mkdir bkgonly_asimov')
    # Dump bkgonly tables - CRs
    os.system('YieldsTable.py -w results/monotop_asimov_bkgonly/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_CRtt,tW1L_CRWm,tW1L_CRWp -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_asimov_CRs.tex -C CRs')
    os.system('pdflatex bkgonly_asimov_CRs.tex')
    # Dump bkgonly tables - VRs
    os.system('YieldsTable.py -w results/monotop_asimov_bkgonly/BkgOnly_combined_NormalMeasurement_model_afterFit.root -c tW1L_VRtt_VR1,tW1L_VRtt_VR2,tW1L_VRW_VR1,tW1L_VRW_VR2 -s Top,SingleTop,Wjets,Zjets,Diboson,ttV,ttH,tWZ -o bkgonly_asimov_VRs.tex -C VRs')
    os.system('pdflatex bkgonly_asimov_VRs.tex')
    # Dump fit parameters plot
    tfile = TFile('results/monotop_asimov_bkgonly/fit_parameters.root')
    fit_parameters = tfile.Get('fit_parameters')
    fit_parameters.SaveAs('fit_parameters_bkgonly_asimov.pdf')
    # Move everything to bkgonly folder
    os.system('mv fit_parameters_bkgonly_asimov.pdf bkgonly_asimov/')
    os.system('mv bkgonly_asimov_* bkgonly_asimov/')

if __name__=='__main__':
    main()    