"""
 **********************************************************************************
 * Project: HistFitter - A ROOT-based package for statistical data analysis       *
 * Package: HistFitter                                                            *
 *                                                                                *
 * Description:                                                                   *
 *      Simple example configuration with input trees                             *
 *                                                                                *
 * Authors:                                                                       *
 *      HistFitter group, CERN, Geneva                                            *
 *                                                                                *
 * Redistribution and use in source and binary forms, with or without             *
 * modification, are permitted according to the terms listed in the file          *
 * LICENSE.                                                                       *
 **********************************************************************************
"""

################################################################
## In principle all you have to setup is defined in this file ##
################################################################

## This configuration performs a simplified version of the "soft lepton" fits documented in ATLAS-CONF-2012-041.
## Only two systematics are considered:
##   -JES (Tree-based) conservatively treated like an MC stat error
##   -Alpgen Kt scale (weight-based)
##
## For the real complete implementation, see: HistFitterUser/MET_jets_leptons/python/MyOneLeptonKtScaleFit_mergerSoftLep.py

from configManager import configMgr
from ROOT import kBlack,kWhite,kGray,kRed,kPink,kMagenta,kViolet,kBlue,kAzure,kCyan,kTeal,kGreen,kSpring,kYellow,kOrange,kDashed,kSolid,kDotted
from configWriter import fitConfig,Measurement,Channel,Sample
from systematic import Systematic
from math import sqrt
from optparse import OptionParser
import sys

import argparse

from ROOT import gROOT, TLegend, TLegendEntry, TCanvas
#gROOT.LoadMacro("./macros/AtlasStyle.C")
import ROOT
#ROOT.SetAtlasStyle()

myUserArgs= configMgr.userArg.split(' ')
 
myInputParser = OptionParser()

myInputParser.add_option('', '--SR', dest = 'SRnum', default = 'SR1')

myInputParser.add_option('', '--blindSR', dest = 'blindSR', default = 'True')
myInputParser.add_option('', '--blindVR', dest = 'blindVR', default = 'True')
myInputParser.add_option('', '--blindCR', dest = 'blindCR', default = 'True')

(options, args) = myInputParser.parse_args(myUserArgs)
whichSR = options.SRnum

doAsimov=False
doValidation=False
if 'asimov' in myUserArgs:
    print('Doing Asimov fit!')
    doAsimov=True
if 'validation' in myUserArgs:
    print('Doing validation - not for use in exclusion fits!')
    doValidation=True    

#---------------------------------------------------------------------------------------------
# Some flags for overridding normal execution and telling ROOT to shut up... use with caution!
#---------------------------------------------------------------------------------------------
#gROOT.ProcessLine("gErrorIgnoreLevel=10001;")
#configMgr.plotHistos = True

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat=True
#doValidation=True #use or use not validation regions to check exptrapolation to signal regions

#-------------------------------
# Parameters for hypothesis test
#-------------------------------
#configMgr.doHypoTest=True
#configMgr.doExclusion=True
configMgr.doUL = False
#configMgr.nTOYs=5000
configMgr.calculatorType=2
configMgr.testStatType=3 #2
configMgr.nPoints=1 #50
configMgr.readFromTree=True
configMgr.scanRange = (0., 2.)

def plot_settings(region, region_name, maxY, ratioMaxY = 1.2, ratioMinY = 0.8):

    region.logY=True
    region.maxY = maxY
    region.ATLASLabelX = 0.2  #AK: for CRs with ratio plot
    region.ATLASLabelY = 0.83
    region.ATLASLabelText = "Internal"
    region.ratioMaxY = ratioMaxY
    region.ratioMinY = ratioMinY
    region.regionLabelX = 0.2
    region.regionLabelY = 0.75
    region.regionLabelText = region_name

#--------------------------------
# Now we start to build the model
#--------------------------------

# First define HistFactory attributes
if doAsimov==True:
    configMgr.analysisName = "monotop_asimov"
else:  
    configMgr.analysisName = "monotop"

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 1. # Luminosity of input TTree after weighting
configMgr.outputLumi = 139. # Luminosity required for output histograms
configMgr.setLumiUnits("fb-1")

configMgr.histCacheFile = "data/"+configMgr.analysisName+".root"

configMgr.outputFileName = "results/"+configMgr.analysisName+"_Output.root"

# Set the files to read from
bgdFiles = []
dataFiles = []
testFiles = []
sigFiles = []

if configMgr.readFromTree:

    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/ttbar_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/singletop_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/wjets_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/ttV_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/zjets_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/diboson_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/ttH_trimmed.root")
    bgdFiles.append("/hepstore/msullivan/WtMET/tuples_summer/tWZ_trimmed.root")

    # Use data for a true background-only fit
    dataFiles.append("/hepstore/msullivan/WtMET/tuples_summer/data_trimmed.root")


    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311091_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311092_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311094_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311095_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311096_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311097_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311205_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311206_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311207_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311208_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311209_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311211_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311212_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311213_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311214_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311215_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311218_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311219_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311220_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311221_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311222_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311232_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311233_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311234_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311235_trimmed.root")
    sigFiles.append("/hepstore/msullivan/WtMET/tuples_summer/311236_trimmed.root")


else:
    bgdFiles = ["data/"+configMgr.analysisName+".root"]

# Dictionary of cuts for Tree->hist

configMgr.cutsDict["SRBin0"] = "(mt >= 200000) && (met > 250000 && met <= 300000) && (amt2 > 220000) && (ReclusteredW_Mass >= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (jet3_pt > 30000) && (bjet2_pt < 50000) "
configMgr.cutsDict["SRBin1"] = "(mt >= 200000) && (met > 300000 && met <= 400000) && (amt2 > 220000) && (ReclusteredW_Mass >= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (jet3_pt > 30000) && (bjet2_pt < 50000) "
configMgr.cutsDict["SRBin2"] = "(mt >= 200000) && (met > 400000 && met <= 500000) && (amt2 > 220000) && (ReclusteredW_Mass >= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (jet3_pt > 30000) && (bjet2_pt < 50000) "
configMgr.cutsDict["SRBin3"] = "(mt >= 200000) && (met > 500000 && met <= 600000) && (amt2 > 220000) && (ReclusteredW_Mass >= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (jet3_pt > 30000) && (bjet2_pt < 50000) "
configMgr.cutsDict["SRBin4"] = "(mt >= 200000) && (met > 600000) && (amt2 > 220000) && (ReclusteredW_Mass >= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (jet3_pt > 30000) && (bjet2_pt < 50000) "

configMgr.cutsDict["tW1L_CRtt"] = "(met >= 250000) && (mt > 200000) && (amt2 < 220000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt > 50000) && (jet3_pt > 30000) "
configMgr.cutsDict["tW1L_CRWm"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000) && (lep1_charge == -1)"
configMgr.cutsDict["tW1L_CRWp"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000) && (lep1_charge == 1)"

if doValidation == True:

    configMgr.cutsDict["tW1L_VRtt_met"] = "(met >= 250000) && (mt > 200000) && (amt2 < 220000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt > 50000) && (jet3_pt > 30000) "
    configMgr.cutsDict["tW1L_VRWm_met"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000) && (lep1_charge == -1)"
    configMgr.cutsDict["tW1L_VRWp_met"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000) && (lep1_charge == 1)"
    configMgr.cutsDict["tW1L_VRtt_lepcharge"] = "(met >= 250000) && (mt > 200000) && (amt2 < 220000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt > 50000) && (jet3_pt > 30000) "
    configMgr.cutsDict["tW1L_VRWm_lepcharge"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000) && (lep1_charge == -1)"
    configMgr.cutsDict["tW1L_VRWp_lepcharge"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000) && (lep1_charge == 1)"

    configMgr.cutsDict["tW1L_VRtt_VR1"] = "(met >= 250000) && (mt > 200000) && (amt2 < 220000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000)"
    configMgr.cutsDict["tW1L_VRtt_VR2"] = "(met >= 250000) && (mt > 200000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000)"

    configMgr.cutsDict["tW1L_VRW_VR1"] = "(met >= 250000) && (mt > 40000) && (mt < 100000) && (amt2 > 220000) && (ReclusteredW_Mass > 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000)"
    configMgr.cutsDict["tW1L_VRW_VR2"] = "(met >= 250000) && (mt > 100000) && (amt2 > 220000) && (ReclusteredW_Mass <= 60000) && (jet1_pt > 50000) && (jet2_pt > 50000) && (bjet2_pt < 50000) && (jet3_pt > 30000)"

# Tuples of nominal weights without and with b-jet selection
configMgr.weights = ('lumi_scale', 'WeightPileUp', 'WeightSF_mu', 'WeightSF_e', 'jvfSF', 'bTagSF', 'WeightEvents', 'WeightEventsSherpa', 'HFScale') 

sigSamples = ['ma_250_mH_500_tb1_st0.7','ma_250_mH_600_tb1_st0.7', 'ma_250_mH_800_tb1_st0.7',
'ma_250_mH_900_tb1_st0.7', 'ma_250_mH_1000_tb1_st0.7', 'ma_250_mH_1250_tb1_st0.7',
'ma_100_mH_500_tb1_st_0.7', 'ma_150_mH_500_tb1_st_0.7', 'ma_200_mH_500_tb1_st_0.7', 
'ma_300_mH_500_tb1_st_0.7', 'ma_350_mH_500_tb1_st_0.7', 'ma_100_mH_600_tb1_st_0.7', 
'ma_150_mH_600_tb1_st_0.7', 'ma_200_mH_600_tb1_st_0.7', 'ma_300_mH_600_tb1_st_0.7',
'ma_350_mH_600_tb1_st_0.7', 'ma_100_mH_800_tb1_st_0.7', 'ma_150_mH_800_tb1_st_0.7', 
'ma_200_mH_800_tb1_st_0.7', 'ma_300_mH_800_tb1_st_0.7', 'ma_350_mH_800_tb1_st_0.7',
'ma_100_mH_1200_tb1_st_0.7', 'ma_150_mH_1200_tb1_st_0.7', 'ma_200_mH_1200_tb1_st_0.7', 
'ma_300_mH_1200_tb1_st_0.7', 'ma_350_mH_1200_tb1_st_0.7']

#--------------------
# List of systematics
#--------------------

# name of nominal histogram for systematics
configMgr.nomName = ""

# List of samples and their plotting colours
#
topSample = Sample("Top", kOrange - 2)
topSample.setStatConfig(useStat)
topSample.setNormFactor("mu_tt",1.,0.,5.)
topSample.setNormRegions([("tW1L_CRtt","cuts"),("tW1L_CRWm","cuts"),("tW1L_CRWp","cuts")])
#
singletopSample = Sample("SingleTop", kOrange + 2)
singletopSample.setStatConfig(useStat)
#
wjetsSample = Sample("Wjets",kCyan + 2)
wjetsSample.setStatConfig(useStat)
wjetsSample.setNormFactor("mu_W",1.,0.,5.)
wjetsSample.setNormRegions([("tW1L_CRtt","cuts"),("tW1L_CRWm","cuts"),("tW1L_CRWp","cuts")])
#
ttVSample = Sample("ttV",kAzure - 3)
ttVSample.setStatConfig(useStat)
#
zjetsSample = Sample("Zjets",kRed - 4)
zjetsSample.setStatConfig(useStat)
#
dibosonSample = Sample("Diboson",kMagenta - 4)
dibosonSample.setStatConfig(useStat)
#
ttHSample = Sample("ttH",kGray)
ttHSample.setStatConfig(useStat)
#
tWZSample = Sample("tWZ",kGray)
tWZSample.setStatConfig(useStat)
#
dataSample = Sample("Data",kBlack)
dataSample.setData()

## set the file from which the samples should be taken
index = 0
for entry in [topSample,singletopSample,wjetsSample,ttVSample,zjetsSample,dibosonSample,ttHSample,tWZSample]:
    temp = []
    fileName = ""
    fileName = str(bgdFiles[index])    
    temp.append(fileName)
    entry.addInputs(temp, "nominal")
    index=index+1

dataSample.addInputs(dataFiles, "nominal")

#************
#Systematics
#************

simple_syst0 = Systematic("syst0", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
simple_syst1 = Systematic("syst1", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
simple_syst2 = Systematic("syst2", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
simple_syst3 = Systematic("syst3", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
simple_syst4 = Systematic("syst4", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")

CR_SingleTop_syst = Systematic("CR_SingleTop_syst", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
CR_Zjets_syst = Systematic("CR_Zjets_syst", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
CR_Diboson_syst = Systematic("CR_Diboson_syst", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
CR_ttV_syst = Systematic("CR_ttV_syst", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
CR_ttH_syst = Systematic("CR_ttH_syst", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")
CR_tWZ_syst = Systematic("CR_tWZ_syst", configMgr.weights, 1+0.2, 1-0.2, "user", "userOverallSys")


#-----------------------------
# Background-only fits 
#-----------------------------

bkt = configMgr.addFitConfig("BkgOnly")
bkt.statErrThreshold=0.0
if doAsimov==True:
    configMgr.blindCR = True
    configMgr.blindVR = True
else:
    configMgr.blindCR = False
    configMgr.blindVR = False

bkt.addSamples([topSample,singletopSample,wjetsSample,ttVSample,zjetsSample,dibosonSample,ttHSample,tWZSample,dataSample])

#bkt.getSample("SingleTop").addSystematic(CR_SingleTop_syst)
#bkt.getSample("Zjets").addSystematic(CR_Zjets_syst)
#bkt.getSample("Diboson").addSystematic(CR_Diboson_syst)
#bkt.getSample("ttV").addSystematic(CR_ttV_syst)
#bkt.getSample("ttH").addSystematic(CR_ttH_syst)
#bkt.getSample("tWZ").addSystematic(CR_tWZ_syst)

tW1L_CRtt = bkt.addChannel("cuts",["tW1L_CRtt"],1,0.5,1.5)
tW1L_CRWm = bkt.addChannel("cuts",["tW1L_CRWm"],1,0.5,1.5)
tW1L_CRWp = bkt.addChannel("cuts",["tW1L_CRWp"],1,0.5,1.5)

if doValidation == True:

    tW1L_VRtt_met = bkt.addChannel("met*0.001",["tW1L_VRtt_met"],5,250,1000)
    tW1L_VRtt_lepcharge = bkt.addChannel("lep1_charge",["tW1L_VRtt_lepcharge"],2,-1.1,1.1)
    tW1L_VRWm_met = bkt.addChannel("met*0.001",["tW1L_VRWm_met"],5,250,1000)
    tW1L_VRWp_met = bkt.addChannel("met*0.001",["tW1L_VRWp_met"],5,250,1000)
    tW1L_VRWp_lepcharge = bkt.addChannel("lep1_charge",["tW1L_VRWp_lepcharge"],2,-1.1,1.1)
    tW1L_VRWm_lepcharge = bkt.addChannel("lep1_charge",["tW1L_VRWm_lepcharge"],2,-1.1,1.1)

    tW1L_VRtt_VR1 = bkt.addChannel("met*0.001",["tW1L_VRtt_VR1"],5,250,1000)
    tW1L_VRtt_VR2 = bkt.addChannel("met*0.001",["tW1L_VRtt_VR2"],5,250,1000)

    tW1L_VRW_VR1 = bkt.addChannel("met*0.001",["tW1L_VRW_VR1"],5,250,1000)
    tW1L_VRW_VR2 = bkt.addChannel("met*0.001",["tW1L_VRW_VR2"],5,250,1000)

    plot_settings(tW1L_VRtt_met, 'tW1L_CRtt', 1E7) 
    plot_settings(tW1L_VRtt_lepcharge, 'tW1L_CRtt', 1E7)
    plot_settings(tW1L_VRWm_met, 'tW1L_CRWm', 1E9)
    plot_settings(tW1L_VRWp_met, 'tW1L_CRWp', 1E9)
    plot_settings(tW1L_VRWp_lepcharge, 'tW1L_CRWp', 1E9)
    plot_settings(tW1L_VRWm_lepcharge, 'tW1L_CRWm', 1E9)
    plot_settings(tW1L_VRtt_VR1, 'tW1L_VRtt_VR1', 1E8)
    plot_settings(tW1L_VRtt_VR2, 'tW1L_VRtt_VR2', 1E8)
    plot_settings(tW1L_VRW_VR1, 'tW1L_VRW_VR1', 1E9)
    plot_settings(tW1L_VRW_VR2, 'tW1L_VRW_VR2', 1E9)

    bkt.addValidationChannels([tW1L_VRtt_met,tW1L_VRWm_met,tW1L_VRWp_met,tW1L_VRtt_lepcharge,tW1L_VRWm_lepcharge,tW1L_VRWp_lepcharge,tW1L_VRtt_VR1,tW1L_VRtt_VR2,tW1L_VRW_VR1,tW1L_VRW_VR2])

bkt.addBkgConstrainChannels([tW1L_CRtt,tW1L_CRWm,tW1L_CRWp])

meas=bkt.addMeasurement(name="NormalMeasurement",lumi=1.0,lumiErr=0.017)
meas.addPOI("mu_SIG")
meas.addParamSetting("mu_BG",True,1)

#-----------------------------
# Exclusion fits 
#-----------------------------

if myFitType==FitType.Exclusion:

    #Setup fit config
    ex = configMgr.addFitConfigClone(bkt,"Exclusion")
    configMgr.blindSR = True
    configMgr.useSignalInBlindedData=False
    meas.addPOI("mu_SIG")

    #Samples
    #ex.addSamples([topSample,singletopSample,wjetsSample,ttVSample,zjetsSample,dibosonSample,ttHSample,tWZSample,dataSample])

    #Systematics

    #Channels
    srbin0 = ex.addChannel("cuts",["SRBin0"],1,0.5,1.5) 
    srbin1 = ex.addChannel("cuts",["SRBin1"],1,0.5,1.5) 
    srbin2 = ex.addChannel("cuts",["SRBin2"],1,0.5,1.5) 
    srbin3 = ex.addChannel("cuts",["SRBin3"],1,0.5,1.5) 
    srbin4 = ex.addChannel("cuts",["SRBin4"],1,0.5,1.5)  

    srbin0.addSystematic(simple_syst0)
    srbin1.addSystematic(simple_syst1)
    srbin2.addSystematic(simple_syst2)
    srbin3.addSystematic(simple_syst3)
    srbin4.addSystematic(simple_syst4)
    #Channel
    ex.addSignalChannels([srbin0, srbin1, srbin2, srbin3, srbin4])

    sigSamples = ['ma_250_mH_500_tb1_st0.7','ma_250_mH_600_tb1_st0.7', 'ma_250_mH_800_tb1_st0.7',
'ma_250_mH_900_tb1_st0.7', 'ma_250_mH_1000_tb1_st0.7', 'ma_250_mH_1250_tb1_st0.7',    
'ma_100_mH_500_tb1_st_0.7', 'ma_150_mH_500_tb1_st_0.7', 'ma_200_mH_500_tb1_st_0.7', 
'ma_300_mH_500_tb1_st_0.7', 'ma_350_mH_500_tb1_st_0.7', 'ma_100_mH_600_tb1_st_0.7', 
'ma_150_mH_600_tb1_st_0.7', 'ma_200_mH_600_tb1_st_0.7', 'ma_300_mH_600_tb1_st_0.7',
'ma_350_mH_600_tb1_st_0.7', 'ma_100_mH_800_tb1_st_0.7', 'ma_150_mH_800_tb1_st_0.7', 
'ma_200_mH_800_tb1_st_0.7', 'ma_300_mH_800_tb1_st_0.7', 'ma_350_mH_800_tb1_st_0.7',
'ma_100_mH_1200_tb1_st_0.7', 'ma_150_mH_1200_tb1_st_0.7', 'ma_200_mH_1200_tb1_st_0.7', 
'ma_300_mH_1200_tb1_st_0.7', 'ma_350_mH_1200_tb1_st_0.7']
 
    index = 0

    for sig in sigSamples:

        myTopLvl = configMgr.addFitConfigClone(ex,"Sig_%s"%sig)

        sigSample = Sample(sig,kPink)
        temp = []
        temp.append(sigFiles[index])
        sigSample.addInputs(temp, "nominal")
        sigSample.setNormByTheory()
        sigSample.setNormFactor("mu_SIG",1.,0.,5.) 
        sigSample.setStatConfig(True)  
        # Uncomment to correlate signal with background systematic
        #sigSample.addSystematic(simple_systX)                
        myTopLvl.addSamples(sigSample)
        myTopLvl.setSignalSample(sigSample)
        index = index+1