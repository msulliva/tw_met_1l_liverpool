#!/bin/bash

HistFitter.py -w -f -F bkg -D "before,after" -u "asimov validation" analysis/Wt_MET_1L.py
mv results/monotop_asimov results/monotop_asimov_bkgonly
HistFitter.py -w -f -F bkg -D "before,after" -u "validation" analysis/Wt_MET_1L.py
mv results/monotop results/monotop_bkgonly

HistFitter.py -w -f -F excl -p -D "before,after" -u "asimov" analysis/Wt_MET_1L.py
mv results/monotop_asimov results/monotop_asimov_excl
HistFitter.py -w -f -F excl -p -D "before,after" analysis/Wt_MET_1L.py
mv results/monotop results/monotop_excl
