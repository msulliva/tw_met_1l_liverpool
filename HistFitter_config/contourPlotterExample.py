#!/usr/bin/env python

# contourPlotterExample.py #################
#
# Example for using contourPlotter.py
#
# See README for details
#
# By: Larry Lee - Dec 2017

import ROOT

import contourPlotter

drawTheorySysts = False

plot = contourPlotter.contourPlotter("contourPlotterExample",800,600)

plot.ATLASlabel = "ATLAS"
plot.ATLASlabel2 = "Internal"
plot.processLabel = "tW+MET 1L"
plot.limitCaveatLabel = "All limits at 95% CL"
discovery = False
plot.lumiLabel = "#sqrt{s}=13 TeV, 139 fb^{-1}"#, All limits at 95% CL"

## Just open up a root file with TGraphs in it so you can hand them to the functions below!

f1 = ROOT.TFile("outputGraphs.root")
f1.ls()

## Axes

plot.drawAxes( [100,500,600,1400] )

## Other limits to draw

# plot.drawShadedRegion( externalGraphs.curve, title="ATLAS 8 TeV, 20.3 fb^{-1} (observed)" )

## Main Result


#plot.drawTextFromTGraph2D( f14.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")

#plot.drawOneSigmaBand(  f14.Get("Band_1s_0")   )
plot.drawExpected(      f1.Get("Exp_0"), title="tW-1L ", color=ROOT.kRed - 0  )
#plot.drawOneSigmaBand(  f1.Get("Band_1s_0")   )

## Axis Labels

plot.setXAxisLabel( "m_{a} [GeV]" )
plot.setYAxisLabel( "m_{H} [GeV]"  )

plot.createLegend(shape=(0.60,0.40,0.9,0.83) ).Draw()

#if drawTheorySysts:
#	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
#	plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
	# coordinate in NDC
#	plot.drawTheoryLegendLines( xyCoord=(0.234,0.6625), length=0.057 )

plot.decorateCanvas( )
plot.writePlot( )




