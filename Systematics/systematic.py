import numpy as np
from ROOT import TFile, TTree, TH1D, TCanvas, gStyle, TLegend

class systematic:

    def __init__(self, files_dict, weights_dict, regions_dict):

        self.files_dict = files_dict
        self.weights_dict = weights_dict
        self.regions_dict = regions_dict
        self.process_name = ''  

    def get_yield(self, inputfile, weight, tree):

        tfile = TFile(inputfile)
        ttree = tfile.Get(tree)
        hist = TH1D('hist','hist',1,-100000,100000)
        hist.Sumw2()
        ttree.Draw('1>>hist',weight)
        print(hist.Integral())
        return hist.GetBinContent(1), hist.GetBinError(1)     

    def uncertainty_yield(self, nominal_file, variation_file, nominal_weight, variation_weight, R, name, tree='tW_nominal', cuts = '1', **kwargs): 

        min_uncert = [1000.0, 1000.0, 1000.0]
        max_uncert = [-1000.0, -1000.0, -1000.0]      

        for nom_file, var_file, nom_weight, var_weight in zip(nominal_file, variation_file, nominal_weight, variation_weight):

            nominal_R = self.get_yield( self.files_dict[nom_file], self.regions_dict[R] + '*' + self.weights_dict[nom_weight] + '*' + cuts, tree)
            variation_R = self.get_yield( self.files_dict[var_file], self.regions_dict[R] + '*' + self.weights_dict[var_weight]  + '*' + cuts, tree)

            systematic = (nominal_R[0] - variation_R[0]) / nominal_R[0] if nominal_R[0] != 0 and variation_R[0] != 0 else -10
            error = variation_R[1] / nominal_R[0] if nominal_R[0] != 0 and variation_R[0] != 0 else -10

            uncertainty = (systematic, error)

            print('{} uncertainty: {} +/- {}'.format(name,round(uncertainty[0], 3),round(uncertainty[1], 3)))

            if systematic > max_uncert[0]:
                max_uncert[0] = systematic
                max_uncert[1] = error
                if nom_file == var_file:
                    max_uncert[2] = systematic
                else:
                    #max_uncert[2] = np.sqrt(systematic**2 + error**2)  
                    max_uncert[2] = systematic

            if systematic < min_uncert[0]:
                min_uncert[0] = systematic
                min_uncert[1] = error  
                if nom_file == var_file:
                    min_uncert[2] = systematic
                else:
                    #min_uncert[2] = np.sqrt(systematic**2 + error**2)
                    min_uncert[2] = systematic

        if (max_uncert[2] == min_uncert[2]):
            min_uncert[2] = -1 * min_uncert[2]  
                    
        with open('output_{}.csv'.format(self.process_name),'a+') as outputcsv:    
            outputcsv.write('{}, {}, {}, {}, {}, {}, {}, {}, {}\n'.format(name, name.split('-')[1], R.replace('_truth',''), max_uncert[2], min_uncert[2], max_uncert[0], min_uncert[0], max_uncert[1], min_uncert[1]))