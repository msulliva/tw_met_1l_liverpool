from ROOT import TFile, TTree, TH1D, gROOT
import multiprocessing as mp
import numpy as np
import sys, argparse
import systematic

#CUTS, WEIGHTS & FILES

common_1L = '(n_lep == 1) * (n_lep_signal == 1) * (met > 250000) * (mt > 40000) * (n_jet > 2) * (jet1_pt > 50000) * (jet2_pt > 50000) * (jet3_pt > 30000) * (bjet1_pt > 50000) * (lep1_pt > 30000) * (PassMetTrigger)'
common_2L = '(Trigger_DiLep) * (n_lep_signal == 2) * (n_lep == 2) * (n_bjet >= 1) * (lep1_charge != lep2_charge) * (lep1_pt/1000. > 25) * (lep1_m != lep2_m || (lep1_m == lep2_m && (mll/1000. > 111 || mll/1000. < 71))) * (mll/1000. > 40) * (bjet1_pt/1000. > 50) * (fabs(dPhimin_4) > 1.1) * (met/1000. > 200) * (minjbl/1000. < 170) * (lep1_match_diLep) * (lep2_match_diLep) * (lep1_truthType==2 || lep1_truthType==6 || lep1_truthType==-1) * (lep2_truthType==2 || lep2_truthType==6 || lep2_truthType==-1)'
common_3L = '(Trigger_DiLep) * (n_lep_signal == 3) * (n_lep == 3) * (n_bjet >= 1) * (bjet1_pt/1000. > 50) * (Zll_candidate) * (lep1_pt/1000. > 25) * (met_corr/1000. > 200) * (mt2_corr/1000. > 90) * ((lep1_match_diLep + lep2_match_diLep + lep3_match_diLep) > 1) * (lep1_truthType == 2 || lep1_truthType == 6 || lep1_truthType == -1) * (lep2_truthType == 2 || lep2_truthType == 6 || lep2_truthType == -1) * (lep3_truthType == 2 || lep3_truthType == 6 || lep3_truthType == -1)'
SFs       = '(xsec*WeightEvents*WeightSF_e*WeightSF_mu*WeightEventsSherpa*bTagSF*WeightPileUp*lumi*jvfSF*PassTruthMetFilter*WeightTrigger_DiLep)/(SumOfWeights)'

common_1L_truth = '(n_lep == 1) * (n_lep_signal == 1) * (met > 250) * (mt > 40) * (n_jet > 2) * (jet1_pt > 50) * (jet2_pt > 50) * (jet3_pt > 30) * (bjet1_pt > 50) * (lep1_pt > 30)'
common_2L_truth = '(n_jet >=1) * (bjet1_pt>50) * (met>60) * (n_lep_signal == 2) * (n_lep == 2) * (lep1_charge == -lep2_charge) * (lep1_pt > 25) * (lep2_pt>20) * (!isSFOS || (mll<71 && mll>111))'

regions_dict = {

	'SR1LBin0':"{} * (amt2 > 220000) * (ReclusteredW_Mass >= 60000) * (mt >= 200000) * (bjet2_pt < 50000) * (met > 250000 && met < 300000)".format(common_1L),
	'SR1LBin1':"{} * (amt2 > 220000) * (ReclusteredW_Mass >= 60000) * (mt >= 200000) * (bjet2_pt < 50000) * (met > 300000 && met < 400000)".format(common_1L),
	'SR1LBin2':"{} * (amt2 > 220000) * (ReclusteredW_Mass >= 60000) * (mt >= 200000) * (bjet2_pt < 50000) * (met > 400000 && met < 500000)".format(common_1L),
	'SR1LBin3':"{} * (amt2 > 220000) * (ReclusteredW_Mass >= 60000) * (mt >= 200000) * (bjet2_pt < 50000) * (met > 500000 && met < 600000)".format(common_1L),
	'SR1LBin4':"{} * (amt2 > 220000) * (ReclusteredW_Mass >= 60000) * (mt >= 200000) * (bjet2_pt < 50000) * (met > 600000)".format(common_1L),

	'SR1LBin0_truth':"{} * (amt2 > 220) * (ReclusteredW_Mass >= 60) * (mt >= 200) * (bjet2_pt < 50) * (met > 250 && met < 300)".format(common_1L_truth),
	'SR1LBin1_truth':"{} * (amt2 > 220) * (ReclusteredW_Mass >= 60) * (mt >= 200) * (bjet2_pt < 50) * (met > 300 && met < 400)".format(common_1L_truth),
	'SR1LBin2_truth':"{} * (amt2 > 220) * (ReclusteredW_Mass >= 60) * (mt >= 200) * (bjet2_pt < 50) * (met > 400 && met < 500)".format(common_1L_truth),
	'SR1LBin3_truth':"{} * (amt2 > 220) * (ReclusteredW_Mass >= 60) * (mt >= 200) * (bjet2_pt < 50) * (met > 500 && met < 600)".format(common_1L_truth),
	'SR1LBin4_truth':"{} * (amt2 > 220) * (ReclusteredW_Mass >= 60) * (mt >= 200) * (bjet2_pt < 50) * (met > 600)".format(common_1L_truth),

	'tW1L_CRtt':"{} * (amt2 < 220000) * (mt >= 200000) * (bjet2_pt > 50000)* (met > 250000)".format(common_1L),
	'tW1L_CRWm':"{} * (lep1_charge == -1) * (amt2 > 220000) * (ReclusteredW_Mass <= 60000) * (mt >= 40000 && mt < 100000) * (bjet2_pt < 50000) * (met > 250000)".format(common_1L),
	'tW1L_CRWp':"{} * (lep1_charge == +1) * (amt2 > 220000) * (ReclusteredW_Mass <= 60000) * (mt >= 40000 && mt < 100000) * (bjet2_pt < 50000) * (met > 250000)".format(common_1L),

	'tW1L_CRtt_truth':"{} * (amt2 < 220) * (mt >= 200) * (bjet2_pt > 50)* (met > 250)".format(common_1L_truth),
	'tW1L_CRWp_truth':"{} * (lep1_charge == +1) * (amt2 > 220) * (ReclusteredW_Mass <= 60) * (mt >= 40 && mt < 100) * (bjet2_pt < 50) * (met > 250)".format(common_1L_truth),
	'tW1L_CRWm_truth':"{} * (lep1_charge == -1) * (amt2 > 220) * (ReclusteredW_Mass <= 60) * (mt >= 40 && mt < 100) * (bjet2_pt < 50) * (met > 250)".format(common_1L_truth),

	'SR2L':'{} * (minmaxmbl/1000. > 150) * (mt2/1000. > 130)'.format(common_2L),
	'tW2L_CRtt':'{} * (minmaxmbl/1000. < 150) * (mt2/1000. > 40) * (mt2/1000. < 80)'.format(common_2L),
	'tW2L_CRttZ':'{} * ((n_bjet > 1 && n_jet > 2 && jet3_pt/1000. > 30) || (n_bjet == 1 && n_jet > 3 && jet4_pt/1000. > 30)) * (minjbl_corr/1000. < 170)'.format(common_3L),
	'tW2L_CRWZ':'{} * (n_bjet == 1) * ((n_jet <= 3) || (n_jet > 3 && jet4_pt/1000. < 30)) * (minjbl_corr/1000. > 170)'.format(common_3L),

	'SR2L_truth':'{} * (minmaxmbl > 150) * (mt2 > 130)'.format(common_2L_truth),
	'tW2L_CRtt_truth':'{} * (minmaxmbl < 150) * (mt2 > 40) * (mt2 < 80)'.format(common_2L_truth)
	#'tW2L_CRttZ_truth':'{} * ((n_bjet > 1 && n_jet > 2 && jet3_pt > 30) || (n_bjet == 1 && n_jet > 3 && jet4_pt > 30)) * (minjbl_corr < 170)'.format(common_3L),
	#'tW2L_CRWZ_truth':'{} * (n_bjet == 1) * ((n_jet <= 3) || (n_jet > 3 && jet4_pt < 30)) * (minjbl_corr. > 170)'.format(common_3L),

}

weights_dict = {

	'nominal':'({})'.format(SFs),

	'ttbar_ISRup':'(m_SystWeights_Var3cUp*m_SystWeights_muR_0p5_muF_0p5*{})/(WeightEvents**2)'.format(SFs),
	'ttbar_ISRdown':'(m_SystWeights_Var3cDown*m_SystWeights_muR_2p0_muF_2p0*{})/(WeightEvents**2)'.format(SFs),
	'ttbar_FSRup':'(m_SystWeights_FSR_muR_1p0_muR_0p5*{})/(WeightEvents)'.format(SFs),
	'ttbar_FSRdown':'(m_SystWeights_FSR_muR_1p0_muR_2p0*{})/(WeightEvents)'.format(SFs),

	'ttbar_truth':'(139000*eventWeight*xsec)/SumOfWeights',

	'ttV_muR0p5_muF0p5':'(m_SystWeights_muR0p5_muF0p5_ttV*{})'.format(SFs),
	'ttV_muR1_muF0p5':'(m_SystWeights_muR1_muF0p5_ttV*{})'.format(SFs),
	'ttV_muR0p5_muF1':'(m_SystWeights_muR0p5_muF1_ttV*{})'.format(SFs),
	'ttV_muR1_muF1':'(m_SystWeights_muR1_muF1_ttV*{})'.format(SFs),
	'ttV_muR2_muF1':'(m_SystWeights_muR2_muF1_ttV*{})'.format(SFs),
	'ttV_muR1_muF2':'(m_SystWeights_muR1_muF2_ttV*{})'.format(SFs),
	'ttV_muR2_muF2':'(m_SystWeights_muR2_muF2_ttV*{})'.format(SFs),

	'top_nominal':'((mcChannelNumber!=410560&&mcChannelNumber!=410408)*{})'.format(SFs),
	'Wt_nominal':'((mcChannelNumber!=410646||mcChannelNumber!=410647||mcChannelNumber!=411036||mcChannelNumber!=411037||mcChannelNumber!=412002||mcChannelNumber!=410654||mcChannelNumber==410655)*{})'.format(SFs),
	't_nominal':'((mcChannelNumber!=410658||mcChannelNumber!=410659||mcChannelNumber!=411032||mcChannelNumber!=411033||mcChannelNumber!=412004)*{})'.format(SFs),
	'top_FSRup':'((mcChannelNumber!=410560&&mcChannelNumber!=410408)*m_SystWeights_FSR_muR_1p0_muR_0p5*SumOfWeights*{})/(WeightEvents*SumOfWeights_FSR_muR1_muR0p5)'.format(SFs),
	'top_FSRdown':'((mcChannelNumber!=410560&&mcChannelNumber!=410408)*m_SystWeights_FSR_muR_1p0_muR_2p0*SumOfWeights*{})/(WeightEvents*SumOfWeights_FSR_muR1_muR2)'.format(SFs),
	'top_ISRup':'((mcChannelNumber!=410560&&mcChannelNumber!=410408)*m_SystWeights_Var3cUp*m_SystWeights_muR_0p5_muF_0p5*SumOfWeights*SumOfWeights*{})/(SumOfWeights_Var3cUp*SumOfWeights_ISR_muR0p5_muF0p5*(WeightEvents**2))'.format(SFs),
	'top_ISRdown':'((mcChannelNumber!=410560&&mcChannelNumber!=410408)*m_SystWeights_Var3cDown*m_SystWeights_muR_2p0_muF_2p0*SumOfWeights*SumOfWeights*{})/(SumOfWeights_Var3cDown*SumOfWeights_ISR_muR2_muF2*(WeightEvents**2))'.format(SFs),

	'V_jets_muR0p5_muF0p5':'(m_SystWeights_muR0p5_muF0p5_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR0p5_muF0p5)'.format(SFs),
	'V_jets_muR0p5_muF1p0':'(m_SystWeights_muR0p5_muF1_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR0p5_muF1)'.format(SFs),
	'V_jets_muR1p0_muF0p5':'(m_SystWeights_muR1_muF0p5_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR1_muF0p5)'.format(SFs),
	'V_jets_muR1p0_muF1p0':'(m_SystWeights_muR1_muF1_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR1_muF1)'.format(SFs),
	'V_jets_muR1p0_muF2p0':'(m_SystWeights_muR1_muF2_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR1_muF2)'.format(SFs),
	'V_jets_muR2p0_muF1p0':'(m_SystWeights_muR2_muF1_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR2_muF1)'.format(SFs),
	'V_jets_muR2p0_muF2p0':'(m_SystWeights_muR2_muF2_Sherpa*SumOfWeights*{})/(WeightEvents*SumOfWeights_muR2_muF2)'.format(SFs),
	'V_jets_ckkw_15':'(ckkw15_Weight*{})'.format(SFs),
	'V_jets_ckkw_30':'(ckkw30_Weight*{})'.format(SFs),
	'V_jets_qsf_025':'(qsf025_Weight*{})'.format(SFs),
	'V_jets_qsf_4':'(qsf4_Weight*{})'.format(SFs)

}

files_dict = {

	'ttbar_nominal':'/hepstore/msullivan/tWMET_tuples/ttbar.root',
	'ttbar_nominal_truth':'/hepstore/msullivan/tWMET_tuples/truth/410470_trimmed.root',
	'ttbar_ME_truth':'/hepstore/msullivan/tWMET_tuples/truth/ttbar_ME.root',
	'ttbar_PS_truth':'/hepstore/msullivan/tWMET_tuples/truth/ttbar_PS.root',
	'ttbar_ME':'/hepstore/msullivan/tWMET_tuples/variation/tt_ME_1L.root',
	#'singletop_nominal':'/hepstore/msullivan/tWMET_tuples/singletop.root',
	'singletop_nominal':'/hepstore/msullivan/tWMET_tuples/systematic/singletop.root',
	'Wt_ME':'/hepstore/msullivan/tWMET_tuples/variation/Wt_ME.root',
	'Wt_PS':'/hepstore/msullivan/tWMET_tuples/variation/Wt_PS.root',
	't_PS':'/hepstore/msullivan/tWMET_tuples/variation/t_PS.root',
	'Wt_int':'/hepstore/msullivan/tWMET_tuples/variation/Wt_int.root',
	#'wjets_nominal':'/hepstore/msullivan/tWMET_tuples/wjets.root',
	#'zjets_nominal':'/hepstore/msullivan/tWMET_tuples/zjets.root',
	'wjets_nominal':'/hepstore/msullivan/tWMET_tuples/systematic/wjets_weights.root',
	'zjets_nominal':'/hepstore/msullivan/tWMET_tuples/systematic/zjets_weights.root',
	#'diboson_nominal':'/hepstore/msullivan/tWMET_tuples/diboson.root',
	'diboson_nominal':'/hepstore/msullivan/tWMET_tuples/systematic/diboson_weights.root',
	'ttV_nominal':'/hepstore/msullivan/tWMET_tuples/new_ttV.root',
	'ttH_nominal':'/hepstore/msullivan/tWMET_tuples/ttH.root'

}

def setup():

	gROOT.SetBatch()
	run()

def run():

	su = systematic.systematic(files_dict, weights_dict, regions_dict)

	dottbar = True
	dottbartruth = True
	dosingletop = True
	dowjets = True
	dozjets = True
	dodiboson = True
	dottV = True
	dottH = False#True

	###### ttbar

	if dottbartruth:

		su.process_name = 'ttbar_1L'
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin0_truth', 'MatrixElement-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin1_truth', 'MatrixElement-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin2_truth', 'MatrixElement-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin3_truth', 'MatrixElement-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin4_truth', 'MatrixElement-Top1L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW1L_CRtt_truth', 'MatrixElement-Top1L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW1L_CRWm_truth', 'MatrixElement-Top1L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW1L_CRWm_truth', 'MatrixElement-Top1L', 'ntuple')

		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin0_truth', 'PartonShower-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin1_truth', 'PartonShower-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin2_truth', 'PartonShower-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin3_truth', 'PartonShower-Top1L', 'ntuple')		
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR1LBin4_truth', 'PartonShower-Top1L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW1L_CRtt_truth', 'PartonShower-Top1L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW1L_CRWp_truth', 'PartonShower-Top1L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW1L_CRWm_truth', 'PartonShower-Top1L', 'ntuple')

		su.process_name = 'ttbar_2L'
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR2L_truth', 'MatrixElement-Top2L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'SR2L_truth', 'PartonShower-Top2L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW2L_CRtt_truth', 'MatrixElement-Top2L', 'ntuple')
		#su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW2L_CRttV_truth', 'MatrixElement-Top2L', 'ntuple')
		#su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_ME_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW2L_CRWZ_truth', 'MatrixElement-Top2L', 'ntuple')
		su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW2L_CRtt_truth', 'PartonShower-Top2L', 'ntuple')	
		#su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW2L_CRtt_truth', 'PartonShower-Top2L', 'ntuple')	
		#su.uncertainty_yield(['ttbar_nominal_truth'], ['ttbar_PS_truth'], ['ttbar_truth'], ['ttbar_truth'], 'tW2L_CRtt_truth', 'PartonShower-Top2L', 'ntuple')	
	
	if dottbar:

		su.process_name = 'ttbar_1L'
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'SR1LBin0', 'FSR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'SR1LBin1', 'FSR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'SR1LBin2', 'FSR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'SR1LBin3', 'FSR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'SR1LBin4', 'FSR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'tW1L_CRtt', 'FSR-Top1L')	
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'tW1L_CRWp', 'FSR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'tW1L_CRWm', 'FSR-Top1L')

		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'SR1LBin0', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'SR1LBin1', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'SR1LBin2', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'SR1LBin3', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'SR1LBin4', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'tW1L_CRtt', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'tW1L_CRWp', 'ISR-Top1L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'], 
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'tW1L_CRWm', 'ISR-Top1L')

		su.process_name = 'ttbar_2L'
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'],
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'SR2L', 'FSR-Top2L')	
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'],
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'SR2L', 'ISR-Top2L')
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'],
		['nominal','nominal'], ['ttbar_FSRup','ttbar_FSRdown'], 'tW2L_CRtt', 'FSR-Top2L')	
		su.uncertainty_yield(['ttbar_nominal','ttbar_nominal'], ['ttbar_nominal','ttbar_nominal'],
		['nominal','nominal'], ['ttbar_ISRup','ttbar_ISRdown'], 'tW2L_CRtt', 'ISR-Top2L')	

	###### ttV
	if dottV:

		su.process_name = 'ttV_1L'

		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'SR1LBin0', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'SR1LBin1', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'SR1LBin2', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'SR1LBin3', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'SR1LBin4', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'tW1L_CRtt', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'tW1L_CRWp', 'muR_muF-ttV')
		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'tW1L_CRWm', 'muR_muF-ttV')
		
		su.process_name = 'ttV_2L'

		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'SR2L', 'muR_muF-ttV')

		su.uncertainty_yield(['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal', 'ttV_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['ttV_muR0p5_muF0p5','ttV_muR0p5_muF1','ttV_muR1_muF0p5','ttV_muR1_muF1','ttV_muR1_muF2','ttV_muR2_muF1','ttV_muR2_muF2'],
		'tW2L_CRttZ', 'muR_muF-ttV')

	###### ttH
	if dottH:

		su.process_name = 'ttH_1L'
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR0p5', 'SR_1L_bin0', 'ttH1L_muR1_muFR0p5_bin0')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR0p5', 'SR_1L_bin1', 'ttH1L_muR1_muFR0p5_bin1')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR0p5', 'SR_1L_bin2', 'ttH1L_muR1_muFR0p5_bin2')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR0p5', 'SR_1L_bin3', 'ttH1L_muR1_muFR0p5_bin3')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR0p5', 'SR_1L_bin4', 'ttH1L_muR1_muFR0p5_bin4')

		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR2', 'SR_1L_bin0', 'ttH1L_muR1_muR2_bin0')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR2', 'SR_1L_bin1', 'ttH1L_muR1_muR2_bin1')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR2', 'SR_1L_bin2', 'ttH1L_muR1_muR2_bin2')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR2', 'SR_1L_bin3', 'ttH1L_muR1_muR2_bin3')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR2', 'SR_1L_bin4', 'ttH1L_muR1_muR2_bin4')

		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadHi', 'SR_1L_bin0', 'ttH1L_RadHi_bin0')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadHi', 'SR_1L_bin1', 'ttH1L_RadHi_bin1')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadHi', 'SR_1L_bin2', 'ttH1L_RadHi_bin2')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadHi', 'SR_1L_bin3', 'ttH1L_RadHi_bin3')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadHi', 'SR_1L_bin4', 'ttH1L_RadHi_bin4')

		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadLo', 'SR_1L_bin0', 'ttH1L_RadLo_bin0')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadLo', 'SR_1L_bin1', 'ttH1L_RadLo_bin1')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadLo', 'SR_1L_bin2', 'ttH1L_RadLo_bin2')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadLo', 'SR_1L_bin3', 'ttH1L_RadLo_bin3')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadLo', 'SR_1L_bin4', 'ttH1L_RadLo_bin4')

		su.process_name = 'ttH_2L'
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR0p5', 'SR2L', 'ttH1L_muR1_muFR0p5_SR2L')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_FSR_muR1_muR2', 'SR2L', 'ttH1L_muR1_muFR2_SR2L')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadHi', 'SR2L', 'ttH1L_RadHi_SR2L')
		su.uncertainty_yield('ttH_nominal', 'ttH_nominal', 'nominal', 'ttbar_RadLo', 'SR2L', 'ttH1L_RadLo_SR2L')
	
	###### Single top
	if dosingletop:

		su.process_name = 'singletop_1L'
		#FSR
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'SR1LBin0','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'SR1LBin1','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'SR1LBin2','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'SR1LBin3','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'SR1LBin4','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'tW1L_CRtt','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'tW1L_CRWp','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'tW1L_CRWm','FSR-SingleTop')
		#ISR
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'SR1LBin0','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'SR1LBin1','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'SR1LBin2','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'SR1LBin3','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'SR1LBin4','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'tW1L_CRtt','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'tW1L_CRWp','ISR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'tW1L_CRWm','ISR-SingleTop')
		#ME
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'SR1LBin0','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'SR1LBin1','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'SR1LBin2','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'SR1LBin3','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'SR1LBin4','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRtt','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRWp','MatrixElement-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_ME'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRWm','MatrixElement-SingleTop')
		#PS
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'SR1LBin0','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'SR1LBin1','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'SR1LBin2','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'SR1LBin3','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'SR1LBin4','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRtt','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRWp','PartonShower-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_PS'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRWm','PartonShower-SingleTop')
		#Interference
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'SR1LBin0','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'SR1LBin1','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'SR1LBin2','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'SR1LBin3','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'SR1LBin4','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRtt','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRWp','Interference-SingleTop')
		su.uncertainty_yield(['singletop_nominal'],['Wt_int'],['Wt_nominal'],['Wt_nominal'],'tW1L_CRWm','Interference-SingleTop')

		su.process_name = 'singletop_2L'
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_FSRup','top_FSRdown'],'SR2L','FSR-SingleTop')
		su.uncertainty_yield(['singletop_nominal','singletop_nominal'],['singletop_nominal','singletop_nominal'],
		['top_nominal','top_nominal'],['top_ISRup','top_ISRdown'],'SR2L','ISR-SingleTop')

	###### V+jets
	if dowjets:

		su.process_name = 'wjets_1L'
		#muR_muF variations
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin0', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin1', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin2', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin3', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin4', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRtt', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRWp', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRWm', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin0', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin1', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin2', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin3', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin4', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'tW1L_CRtt', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'tW1L_CRWp', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'tW1L_CRWm', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin0', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin1', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin2', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin3', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin4', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'tW1L_CRtt', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'tW1L_CRWp', 'qsf-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'tW1L_CRWm', 'qsf-Wjets')

		su.process_name = 'wjets_2L'
		#muR_muF variations
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal', 'wjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR2L', 'muR_muF-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR2L', 'ckkw-Wjets')
		su.uncertainty_yield(['wjets_nominal', 'wjets_nominal'], ['wjets_nominal', 'wjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR2L', 'qsf-Wjets')
	
	if dozjets:

		su.process_name = 'zjets_1L'
		#muR_muF variations
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin0', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin1', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin2', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin3', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin4', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRtt', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRWp', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRWm', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin0', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin1', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin2', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin3', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR1LBin4', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'tW1L_CRtt', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'tW1L_CRWp', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'tW1L_CRWm', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin0', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin1', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin2', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin3', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR1LBin4', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'tW1L_CRtt', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'tW1L_CRWp', 'qsf-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'tW1L_CRWm', 'qsf-Zjets')

		su.process_name = 'zjets_2L'
		#muR_muF variations
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal', 'zjets_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR2L', 'muR_muF-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_ckkw_15','V_jets_ckkw_30'],
		'SR2L', 'ckkw-Zjets')
		su.uncertainty_yield(['zjets_nominal', 'zjets_nominal'], ['zjets_nominal', 'zjets_nominal'], 
		['nominal','nominal'], ['V_jets_qsf_025','V_jets_qsf_4'],
		'SR2L', 'qsf-Zjets')		

	###### Diboson
	if dodiboson:

		su.process_name = 'diboson_1L'

		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin0', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin1', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin2', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin3', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR1LBin4', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRtt', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRWp', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW1L_CRWm', 'muR_muF-Diboson')

		su.process_name = 'diboson_2L'

		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'SR2L', 'muR_muF-Diboson')
		su.uncertainty_yield(['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal', 'diboson_nominal'],
		['nominal','nominal','nominal','nominal','nominal','nominal','nominal'],
		['V_jets_muR0p5_muF0p5','V_jets_muR0p5_muF1p0','V_jets_muR1p0_muF0p5','V_jets_muR1p0_muF1p0','V_jets_muR1p0_muF2p0','V_jets_muR2p0_muF1p0','V_jets_muR2p0_muF2p0'],
		'tW2L_CRWZ', 'muR_muF-Diboson')

if __name__=='__main__':
	setup()	

		






