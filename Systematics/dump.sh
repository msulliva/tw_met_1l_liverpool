#!/bin/bash

rm *.txt *.png

python3 plot_new.py output_ttbar_1L.csv 0 
python3 plot_new.py output_ttbar_2L.csv 0 
python3 plot_new.py output_ttV_1L.csv 0 0
python3 plot_new.py output_ttV_2L.csv 0 0
python3 plot_new.py output_singletop_1L.csv 0 
python3 plot_new.py output_diboson_1L.csv 0
python3 plot_new.py output_diboson_2L.csv 0
python3 plot_new.py output_wjets_1L.csv 0 
python3 plot_new.py output_wjets_2L.csv 0 
python3 plot_new.py output_zjets_1L.csv 0 
python3 plot_new.py output_zjets_2L.csv 0 

for file in $PWD/*.tex
do
    pdflatex $file
done    
