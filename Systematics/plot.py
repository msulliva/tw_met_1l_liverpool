import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys
import os

def main():

    if len(sys.argv) < 2:
        sys.exit('Please specify a file!')
    elif len(sys.argv) == 2:
        filename = sys.argv[1]
        process = filename.split('output_')[1].split('.csv')[0]
        splitbyregion = False
        totalerror = True
    elif len(sys.argv) == 3:
        filename = sys.argv[1]
        process = filename.split('output_')[1].split('.csv')[0]
        splitbyregion = False if sys.argv[2] == '0' else True  
        totalerror = True     
    elif len(sys.argv) == 4:
        filename = sys.argv[1]
        process = filename.split('output_')[1].split('.csv')[0]
        splitbyregion = False if sys.argv[2] == '0' else True  
        totalerror = False if sys.argv[3] == '0' else True      

    df = pd.read_csv(filename,names=['name','process','region','valueup','valuedown','systup','systdown','statup','statdown'])
    regions = []
    for region in df['region']:
        if region not in regions and splitbyregion:
            regions.append(region)
        elif not splitbyregion and len(regions) == 0:
            regions.append('')          

    for region in regions:
        plot(df, process, region, totalerror)

def plot(df, process, region, totalerror):

    tempdf = df[df['region'].str.contains(region)]
    #tempdf = tempdf.append(envelope(tempdf)).reset_index(drop=True)
    #tempdf = tempdf[~tempdf['region'].str.contains('env:')].reset_index(drop=True)
    table(tempdf, process, region)
    y_values = np.linspace(0, tempdf.shape[0] - 1, tempdf.shape[0])
    y_ticks = []
    for entry in y_values:
        tick = '{}_{}'.format(tempdf.iloc[entry]['name'], tempdf.iloc[entry]['region'])
        y_ticks.append(tempdf.iloc[entry]['name'])
    plt.yticks(y_values, '{}_{}'.format(tempdf['name'],tempdf['region']))
    plt.grid(which='both',axis='x')
    plt.xlabel('Uncertainty (%)')
    #plt.margins(0.2)
    plt.subplots_adjust(left=0.4)
    plt.xlim(-100,100)
    #plt.scatter(np.zeros(len(y_values)), y_values)
    plt.hlines( y_values, tempdf['valuedown']*100., tempdf['valueup']*100.,color='tab:blue')
    #plt.scatter(tempdf['value']*100., y_values, s=20)
    plt.savefig('plot_{}_{}.png'.format(process, region))
    plt.gcf().clear()      

def envelope(tempdf):

    tempdf = tempdf[tempdf['region'].str.contains('env:')].reset_index(drop=True)
    newdf = pd.DataFrame(columns = tempdf.columns)
    if tempdf.size > 0:
        maxname  = tempdf.loc[tempdf['value'].idxmax()]['region']
        maxvalue = tempdf.loc[tempdf['value'].idxmax()]['value']
        maxerror = tempdf.loc[tempdf['value'].idxmax()]['stat']
        minname  = tempdf.loc[tempdf['value'].idxmin()]['region']
        minvalue = tempdf.loc[tempdf['value'].idxmin()]['value']
        minerror = tempdf.loc[tempdf['value'].idxmin()]['stat']

        newdf.at[0,'region']  = maxname.replace('env:','')
        newdf.at[0,'value'] = maxvalue
        newdf.at[0,'stat']  = maxerror
        newdf.at[1,'region']  = minname.replace('env:','')
        newdf.at[1,'value'] = minvalue
        newdf.at[1,'stat']  = minerror

    return newdf

def table(tempdf, process, region):

    process = process.replace(' ','')#.replace('_','\\_')
    region  = region.replace(' ','')#.replace('_','\\_')
    filename = 'table_{}_{}.tex'.format(process, region)#.replace('\\','')
    table = open(filename,'w+')   
    table.write('\\documentclass{article}\n')
    table.write('\\begin{document}\n')
    table.write('\\begin{table}\n')
    table.write('\\centering\n')
    table.write('\\begin{tabular}{c|cc}\n')
    table.write('Source & Variation up (Stat. up) \\% & Variation down (Stat. down) \\% \\\\ \\hline \n')

    for entry in range(tempdf.shape[0]):

        name = tempdf.iloc[entry]['name'].replace('_','\\_')
        name += '_{}'.format(tempdf.iloc[entry]['region']).replace('_','\\_').replace(' ','')
        valueup = tempdf.iloc[entry]['valueup']*100.
        statup  = tempdf.iloc[entry]['statup']*100.
        systup  = tempdf.iloc[entry]['systup']*100.
        
        valuedown = tempdf.iloc[entry]['valuedown']*100.
        statdown  = tempdf.iloc[entry]['statdown']*100.
        systdown  = tempdf.iloc[entry]['systdown']*100.

        #if totalerror:
        #    line = '{} & {} $\\pm$ {} & {} \\\\ \n'.format(name, round(value, 2), round(error, 2), np.sqrt(value**2 + error**2))
        #else:
        #    line = '{} & {} & {} \\\\ \n'.format(name, round(value, 2), round(value, 2))  

        #print('{}, {}, {}'.format(name, valueup, valuedown))

        line = ''
        if (valueup == systup and valuedown == systdown):
            line = '{} & {} & {} \\\\ \n'.format(name, round(valueup, 4), round(valuedown, 4))
        elif (valueup != systup and valuedown != systdown):
            line = '{} & {} ({}) & {} ({}) \\\\ \n'.format(name, round(valueup, 4), round(statup, 4), round(valuedown, 4), round(statdown, 4))
        table.write(line)

    table.write('\\end{tabular}\n')
    table.write('\\end{table}\n')
    table.write('\\end{document}\n')    
    #os.system('pdflatex {}'.format(filename))


if __name__=='__main__':
    main()